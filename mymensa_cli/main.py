#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import datetime
import logging
import os
import stat
import configparser
import requests
from bs4 import BeautifulSoup
import unicodedata
import json
from colorama import Fore, Back, Style

VERSION = "0.3.1"
replace = {
        "augsburg": {
                "12": '🐖',
                "17": '🐔',
                "14": '🍷',
                "15": "🐄",
                "21": 'mensaVital',
                "Ei": "🥚",
                "Fi": "🐟",
                "La": "🥛",
                "Gl": "🌾",
                "Gl1": "🌾1",
                "Nu": "🥜",
                "Nu4": "🥜4"
            }
        }


class MymensaCli:
    def __init__(self, url: str, mensa:str, emojisupport:bool, emojibinding:str):
        self.url = url
        self.mensa = mensa
        self.emojisupport = emojisupport
        self.emojibinding = emojibinding

    def run(self):
        self._parse_args()
        fullurl = self._build_url()
        text = self._return_html_text(fullurl)
        soup = self._return_soup(text)
        parsed_dir = self._parse_soup(soup)
        mensatitle = soup.find("h3", {"class":"mensatitle"}).text.strip()
        print(Fore.GREEN,20*" "+mensatitle, Fore.RESET)
        today_string = self.datum.strftime("%Y-%m-%d")
        print(20*" ",(int((len(mensatitle)-len(today_string))/2))*" ",today_string)
        self._print_parsed_dir(parsed_dir)

    def _print_parsed_dir (self, parsed_dir):
        for key, value in parsed_dir.items():
            print(Fore.BLUE+"-"+key,Fore.RESET)
            for x in value:
                print("    "+Fore.GREEN+x["name"], Fore.RESET)
                print("    "+Fore.YELLOW+x["price"]+Fore.RESET)
                if self.emojisupport in ["True", "true", "1"]:
                    for idx,i in enumerate(x["ref"]):
                        if i in replace[self.emojibinding].keys():
                            x["ref"][idx] = replace[self.emojibinding][i]
                print("    "+" ".join(x["ref"]))
            print("")


    def _parse_soup(self, soup) -> dict:
        today_dir = soup.find("div", {"data-date2": self.datum.strftime("%Y-%m-%d")})
        #unsorted_list = today_dir.find("ul", {"data-role":"listview"})
        elements = today_dir.select("ul.checkgroupdividers > li")
        #dishes = today_dir.select("ul.checkgroupdividers > li.conditional")        
        #headlin2 = [x.text.strip() for x in headlines]
        lastHeadline = ""
        foodDict = {}
        for item in elements:
            if "groupdivider" in item.get("class"):
                lastHeadline = item.text
                foodDict[lastHeadline] = []
            elif "conditional" in  item.get("class"):
                foodDict[lastHeadline].append({"name": item.h3.get_text(strip=True).replace("­", ""), "price": item.select_one("p.next").text, "ref": json.loads(item["ref"])})
        return foodDict


    def _build_url(self) -> str:
        return self.url+"/essen.php?mensa="+self.mensa

    def _return_html_text(self, fullurl) -> str:
        requ  = requests.get(fullurl)
        return requ.content

    def _return_soup (self, text) -> BeautifulSoup:
        soup = BeautifulSoup(text, "html.parser")
        return soup

    def _parse_args(self):
        parser = self._create_parser()
        args = parser.parse_args()
        if args.datum:
            self.datum = datetime.datetime.strptime(args.datum+str(datetime.datetime.now().year), "%d.%m.%Y")
        else:
            self.datum = datetime.datetime.today()

    def _create_parser(self):
        parser = argparse.ArgumentParser()
        parser.epilog = "version " + VERSION
        parser.description = "Kommandozeilen-Client für MyMensa"
        parser.add_argument("--datum", "-d",
                            help="date of the canteen-menu in the format 31.12. (DD.MM.) "
                                 "(Standard: heute)")
        return parser 

class Configuration:
    def __init__(self):
        self.configfile = os.path.expanduser('~/.config/mymensa-cli.ini')
        self.config = configparser.ConfigParser()
        self._create_default_if_missing()

        if ('url' not in self.config['Server']) or (self.config['Server']['url'] == ""):
            logging.debug("No url found in config file. Asking user")
            self.config['Server']['url'] = \
                input("URL eingeben:")
        
        if ('mensa' not in self.config['Server']) or (self.config['Server']['mensa'] == ""):
            logging.debug("No mensa found in config file. Asking user")
            self.config['Server']['mensa'] = \
                input("Mensa-Id eingeben:")
            with open(self.configfile, 'w') as f:
                self.config.write(f)

    def _create_default_if_missing(self):
        if not os.path.isfile(self.configfile):
            logging.debug("No config file found. Creating %s", self.configfile)
            self.config['Server'] = {
                "url": "",
                "mensa":""
            }
            self.config['Emoji'] = {
                "emojisupport": True,
                "emojibinding": "augsburg"
            }
            with open(self.configfile, 'w') as f:
                self.config.write(f)

            # change file permissions to 600
            os.chmod(self.configfile, stat.S_IRUSR | stat.S_IWUSR)

            print("Passe die Einträge in der Konfigurationsdatei an: ",
                  self.configfile)
            exit(0)

        else:
            self.config.read(self.configfile)



def main():
    config = Configuration()
    svr = config.config["Server"]
    emo = config.config["Emoji"]

    mcli = MymensaCli(svr["url"], svr["mensa"], emo["emojisupport"], emo["emojibinding"])
    mcli.run()

if __name__ == "__main__":
    main()
