# mymensa_cli
CLI-Tool for displaying the menu of german university canteens, which parses the HTML of the [city].my-mensa.de page.

## Getting Started
### Prerequirements
The project is written in python3, make sure it is installed on your system.
Developed on Arch, linux should work fine, no idea about mac/brew or windows yet.

### Installation
```bash
pip3 install git+https://gitlab.com/craftamap/mymensa_cli
```


### Configuration
The configuration is saved in `~/.config/mymensa-cli.ini`.

On the first start of the application, you will be asked for the URL of your "my-mensa"-page. Please enter it in following format:
```
https://[city].my-mensa.de
```
as example:
```
https://augsburg.my-mensa.de
```
**Important:** There is no url-validation yet, so please use the right format: with https, without a / on the end

The software also asks for your mensa. Your mensa-id you have to enter is saved in the url of your web-instance: `https://augsburg.my-mensa.de/essen.php?v=5141062&hyp=1&lang=de&mensa=aug_friedbergerstr_fh#aug_friedbergerstr_fh_tag_2018318`, the mensa-id is aug_friedbergerstr_fh


####Emoji-Support
mymensa_cli supports emojis in order to replace incredients with emojis. On default, emoji-support is enabled with the "augsburg"-replacement map (currently the only replacement map). If you don't want to use emojis, just disable them in the config.

## Usage

The script supports commandline arguments.
```
optional arguments:
  -h, --help            show this help message and exit
  --datum DATUM, -d DATUM
                        date of the canteen-menu in the format 31.12. (DD.MM.)
                        (Standard: heute)

```
## Contributing
A contribution guide is following soon.

## Tested "Studentenwerke"/cities
for build 0.1:
Works:
* Studentenwerk Augsburg / [augsburg.my-mensa.de](https://augsburg.my-mensa.de)
* Studierendenwerk Münster / [muenster.my-mensa.de](https://muenster.my-mensa.de)

Doesn't work:
* München
* Berlin

Has Emoji-Replacement-Map:
* Augsburg

Please test your university and report, if it works or not!

## Authors 
* **Fabian Siegel** - *Initial work* - [craftamap](https://gitlab.com/craftamap)

## Licence
Coming soon
